import React from "react";
 

function Login(props) {
  const onSubmit = () => {
    localStorage.setItem('token', 'randomValue');
    props.history.push('/profile')
  }

  return (
    <div className='base'>
      <form className='login'>
      <h1>Login</h1>
      <div>
        <div>Email</div>
        <div><input type="email" /></div>
      </div>
      <div>
        <div>Password</div>
        <div><input type="password" /></div>
      </div>
      <div className='btn'>
        {/* {/ <Link to="/register">Register</Link> /}
        */}
        <button onClick={onSubmit} >Submit</button>
      </div>
    </form>
    </div>
  );
}

export default Login;
