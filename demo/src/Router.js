import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Login from './Login';


// import FourZeroFour from './FourZeroFour';
import Profile from './component/Profile';
import About from './About';
// import Direct from './component/Direct';


const PrivateRoute = (props) => {
    const token = localStorage.getItem('token');
    if (token) {
        return <Route exact={true} path={props.path} component={props.component} />
    } else {
        return <Login {...props} />
    }
}

const Router = (props) => {
    return <Switch>
         {/* <Direct /> */}
        <Route path="/" component={Login} exact={true} />
        <Route path="/login" component={Login} />
        <Route path="/about" component={About} />
        
        <PrivateRoute path="/profile" component={Profile} />
        {/* <PrivateRoute path="/profile" component={Profile} /> */}

        {/* <Route component={FourZeroFour} /> */}
    </Switch>
}

export default Router;
