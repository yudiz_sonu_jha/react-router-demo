import React, { Component } from 'react'
function About() {
  return (
   
       <div className='about'>
        <form className='form1'>
            <h1>About React Router Dom</h1>  
            <p>To get started with React Router in a web app, you’ll need a React web app. If you need to create one, we recommend you try Create React App. It’s a popular tool that works really well with React Router.

First, install create-react-app and make a new project with it.

npx create-react-app demo-app
cd demo-app
Installation
You can install React Router from the public npm registry with either npm or yarn. Since we’re building a web app, we’ll use react-router-dom in this guide.

yarn add react-router-dom</p>  
        </form>
    </div>

  )
}

export default About
